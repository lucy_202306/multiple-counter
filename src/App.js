import { Provider } from "react-redux";
import "./App.css";
import MultioleCounter from "./components/MultipleCounter";
import store from "./app/store";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <MultioleCounter></MultioleCounter>
      </div>
    </Provider>
  );
}

export default App;
