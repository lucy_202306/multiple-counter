import { useDispatch } from "react-redux";
import { decrement, increment } from "./CounterSlice";

const Counter = (props) => {

  const dispatch = useDispatch()

  const handleIncrease = () => {
    dispatch(increment(props.index))
  };

  const handleDecrease = () => {
    dispatch(decrement(props.index))
  };

  return (
    <div>
      <button onClick={handleIncrease}>+</button>
      <span>{props.value}</span>
      <button onClick={handleDecrease}>-</button>
    </div>
  );
};

export default Counter;
