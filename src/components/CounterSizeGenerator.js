import { useDispatch, useSelector } from "react-redux";
import { incrementByAmount } from "./CounterSlice";

const CounterSizeGenerator = () => {

    const counterList = useSelector((state) => state.counter.counterList)
    const size = counterList.length
    const dispatch = useDispatch()

    const handleChange = (e) => {
        const size = Number(e.target.value);
        if(size < 0) {
            dispatch(incrementByAmount(0))
            return;
        }
        dispatch(incrementByAmount(size))
    }

    return (
        <div>
            size: <input type="number" value={size} onChange={handleChange}></input>
        </div>
    )
}

export default CounterSizeGenerator;