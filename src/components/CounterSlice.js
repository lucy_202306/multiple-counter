import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    counterList: [1,2,3,4],
  },
  reducers: {
    increment: (state, action) => {
      state.counterList[action.payload] += 1
    },
    decrement: (state, action) => {
    state.counterList[action.payload] -= 1
    },
    incrementByAmount: (state, action) => {
        state.counterList = Array(action.payload).fill(0)
    },
  },
})


export const { increment, decrement, incrementByAmount } = counterSlice.actions

export default counterSlice.reducer