import CounterGroup from "./CounterGroup";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroupSum from "./CounterGroupSum";

const MultioleCounter = () => {

    return (
        <div>
            <CounterSizeGenerator></CounterSizeGenerator>
            <CounterGroupSum></CounterGroupSum>
            <CounterGroup></CounterGroup>
        </div>
    )
}

export default MultioleCounter;